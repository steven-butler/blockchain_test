# -*- coding: utf-8 -*-
import traceback
from uuid import uuid4

from flask import Flask, jsonify, make_response, render_template
from flask import request

from coin import Blockchain

app = Flask(__name__)
blockchain = Blockchain()
# Generate a globally unique address for this node
node_identifier = str(uuid4()).replace('-', '')


@app.route('/')
def index():
    return render_template(
        "index.html",
        node_id=node_identifier
    )


@app.route('/nodes/register', methods=['POST'])
def register_nodes():
    values = request.get_json()

    nodes = values.get('nodes')
    if nodes is None:
        return "Error: Please supply a valid list of nodes", 400

    for node in nodes:
        blockchain.register_node(node)

    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(blockchain.nodes),
    }
    return jsonify(response), 201


@app.route('/nodes/resolve', methods=['GET'])
def consensus():
    replaced = blockchain.resolve_conflicts()

    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': blockchain.chain_json
        }
    else:
        response = {
            'message': 'Our chain is authoritative',
            'chain': blockchain.chain_json
        }

    return jsonify(response), 200


@app.route('/transact', methods=['POST'])
def transaction():
    """
    Create a new transaction on the blockchain
    :return:
    """
    transaction = request.get_json()

    required_values = ['sender', 'recipient', 'amount']
    if not all(value in transaction for value in required_values):
        return make_response(jsonify(status="error", detail="Missing required values"), 400)

    sender = transaction['sender']
    recipient = transaction['recipient']
    amount = transaction['amount']

    try:
        blockchain.new_transaction(sender, recipient, amount)
        return make_response(jsonify(status="success"), 201)
    except Exception as ex:
        print(ex, traceback.format_exc())
        return make_response(jsonify(status="error", detail="Couldn't create transaction"), 500)


@app.route('/chain', methods=['GET'])
def get_chain():
    """
    Get the blocks on the chain
    :return:
    """

    return make_response(jsonify(chain=blockchain.chain_json), 200)


@app.route('/block/last', methods=['GET'])
def get_last_block():
    """
    Get the blocks on the chain
    :return:
    """

    return make_response(jsonify(block=blockchain.last_block.json), 200)


@app.route('/mine', methods=['GET'])
def mine():
    """
    Do the mining
    :return:
    """
    # We run the proof of work algorithm to get the next proof...
    last_block = blockchain.last_block
    proof = blockchain.proof_of_work(last_block)

    # We must receive a reward for finding the proof.
    # The sender is "0" to signify that this node has mined a new coin.
    blockchain.new_transaction(
        sender="0",
        recipient=node_identifier,
        amount=1,
    )

    # Forge the new Block by adding it to the chain
    previous_hash = blockchain.hash(last_block)
    block = blockchain.new_block(proof, previous_hash)

    response = {
        'message': "New Block Phorged",
        'index': block.index,
        'data': block.data,
        'proof': block.proof,
        'previous_hash': block.previous_hash,
    }
    return make_response(jsonify(response), 200)
