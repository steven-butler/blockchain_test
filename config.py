# -*- coding: utf-8 -*-

from pathlib import Path
from dotenv import load_dotenv

import os


# load env file ########################################################################################################
env_path = Path(".") / ".env"

if not os.path.isfile(env_path):
    env_path = Path(".") / "dev.env"

load_dotenv(dotenv_path=env_path)

DEBUG = bool(os.getenv("DEBUG"))

basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = os.getenv("SECRET_KEY")
